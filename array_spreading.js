// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = [...west, "Gill", "Brian", "Noel", "Maggie"]
// console.log(east)

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];

//Driver Code
console.log(combined)